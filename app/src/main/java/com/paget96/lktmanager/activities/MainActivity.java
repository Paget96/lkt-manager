package com.paget96.lktmanager.activities;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.appbar.MaterialToolbar;
import com.paget96.lktmanager.R;
import com.paget96.lktmanager.fragments.FragmentAbout;
import com.paget96.lktmanager.fragments.FragmentMain;
import com.paget96.lktmanager.utils.Utils;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener,
        FragmentManager.OnBackStackChangedListener {

    private final Utils utils = new Utils();
    // Variables
    private boolean doubleBackPressed;

    private void initializeToolbar() {
        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main);

        initializeToolbar();

        //Listen for changes in the back stack
        getSupportFragmentManager().addOnBackStackChangedListener(this::shouldDisplayHomeUp);

        if (savedInstanceState == null) {
            replaceFragment(new FragmentMain(), false, true);
        }
    }

    public void shouldDisplayHomeUp() {
        // Enable Up button only if there are entries in the back stack
        boolean canBack = getSupportFragmentManager().getBackStackEntryCount() > 0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canBack);
        getSupportActionBar().setHomeButtonEnabled(canBack);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    @Override
    public void onBackPressed() {
        if (!doubleBackPressed && getSupportFragmentManager().getBackStackEntryCount() == 0) {
            this.doubleBackPressed = true;
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(() -> doubleBackPressed = false, 2000);
        } else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        utils.closeShell();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }

    public void replaceFragment(Fragment newFragment, boolean addToStack, boolean transitionAnimations) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (addToStack) {
            if (transitionAnimations)
                fragmentTransaction.setCustomAnimations(R.anim.fragment_open_enter, R.anim.fragment_fade_exit);

            fragmentTransaction.replace(R.id.fragment_container, newFragment)
                    .addToBackStack(null).commit();

        } else {
            if (transitionAnimations)
                fragmentTransaction.setCustomAnimations(R.anim.fragment_open_enter, R.anim.fragment_fade_exit);

            fragmentTransaction.replace(R.id.fragment_container, newFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.overflow_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_settings:
                // Settings
                Toast.makeText(MainActivity.this, "Nothing in here", Toast.LENGTH_SHORT).show();
                //replaceFragment(new FragmentSettings(), true, true);
                return true;

            case R.id.action_about:
                // About
                replaceFragment(new FragmentAbout(), true, true);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}