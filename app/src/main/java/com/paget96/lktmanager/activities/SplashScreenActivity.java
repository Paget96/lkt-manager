package com.paget96.lktmanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.paget96.lktmanager.R;
import com.topjohnwu.superuser.Shell;

public class SplashScreenActivity extends AppCompatActivity {

    // Variables
    private ImageView appImageView;
    private boolean root;

    private void initializeViews() {
        appImageView = findViewById(R.id.app_icon);
    }

    private void animateIcon() {
        // Set up a animation and start background work
        // after the animation is finished
        Animation fadeIn;
        fadeIn = AnimationUtils.loadAnimation(SplashScreenActivity.this, R.anim.bounce);
        appImageView.setAnimation(fadeIn);

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                root = Shell.rootAccess();

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (root) {
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                } else
                    Toast.makeText(SplashScreenActivity.this, "No root!", Toast.LENGTH_SHORT).show();

                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_splash_screen);

        initializeViews();
        animateIcon();
    }

}
