package com.paget96.lktmanager.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.paget96.lktmanager.BuildConfig;
import com.paget96.lktmanager.R;
import com.paget96.lktmanager.activities.MainActivity;
import com.paget96.lktmanager.databinding.FragmentMainBinding;
import com.paget96.lktmanager.utils.Utils;

import java.util.Locale;

public class FragmentMain extends Fragment {

    private final Handler periodicUiUpdate = new Handler();
    private final Utils utils = new Utils();
    // Variables
    private FragmentMainBinding binding;
    private final Runnable periodicUpdate = new Runnable() {
        @Override
        public void run() {
            updateUI();

            periodicUiUpdate.postDelayed(periodicUpdate, 1000);
        }
    };

    private void initializeViews() {
        binding.ramUsage.setProgressColor(ContextCompat.getColor(getActivity(), R.color.dark_color_primary));
        binding.ramUsage.setTextColor(ContextCompat.getColor(getActivity(), R.color.dark_color_primary));
    }

    private void updateUI() {
        long totalRam = utils.totalRam(getActivity()) / 1024 / 1024;
        long usedRam = utils.usedRam(getActivity()) / 1024 / 1024;

        float progress = (float) usedRam / (float) totalRam * 100f;

        binding.ramUsageText.setText(String.format(Locale.ENGLISH, "%dMB/%dMB", usedRam, totalRam));
        binding.ramUsage.setProgress((int) progress);

        binding.cpuTemperature.setText(String.format(Locale.US, "%.1f °C", utils.parseWithDefault(utils.readFile("sys/class/thermal/thermal_zone0/temp", true), 0f) / 1000f));
    }

    private void viewsWorkout() {
        binding.log.setOnClickListener(v -> ((MainActivity) getActivity()).replaceFragment(new FragmentLogView(), true, true));

        binding.boost.setOnClickListener(v -> new Thread(() -> {

            // Variables
            long totalRam, usedRam, freeRam;
            long before, after, result;

            before = utils.usedRam(getActivity());
            utils.runCommand("sync; " +
                    "sysctl -w vm.drop_caches=3; " +
                    "sysctl -w vm.shrink_memory=1", true);
            after = utils.usedRam(getActivity());
            result = (before - after) / 1024;

            // Get free ram
            totalRam = utils.totalRam(getActivity());
            usedRam = utils.usedRam(getActivity());
            freeRam = (totalRam - usedRam) / 1024;


            getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), getActivity().getString(R.string.memory_freed_up, (result <= 0 ? "0" : String.valueOf(result / 1024)), String.valueOf(freeRam / 1024)), Toast.LENGTH_LONG).show());

        }).start());

        binding.profilePowersave.profileName.setText(getActivity().getString(R.string.power_save));
        binding.profilePowersave.profileDescription.setText(getActivity().getString(R.string.power_save_exp));
        binding.profilePowersave.profileIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_battery));
        binding.profilePowersave.getRoot().setOnClickListener(v -> {
            binding.profilePowersave.applyingProfileDialog.setVisibility(View.VISIBLE);
            binding.profilePowersave.applyingProfileText.setText(getActivity().getString(R.string.power_save_set_up));

            utils.runCommand("lkt 1 &", true);

            new Handler().postDelayed(() -> {
                binding.profilePowersave.applyingProfileDialog.setVisibility(View.GONE);
                profileState();
            }, 2000);
        });

        binding.profileBalanced.profileName.setText(getActivity().getString(R.string.balanced));
        binding.profileBalanced.profileDescription.setText(getActivity().getString(R.string.balanced_exp));
        binding.profileBalanced.profileIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_balance));
        binding.profileBalanced.getRoot().setOnClickListener(v -> {
            binding.profileBalanced.applyingProfileDialog.setVisibility(View.VISIBLE);
            binding.profileBalanced.applyingProfileText.setText(getActivity().getString(R.string.balanced_set_up));

            utils.runCommand("lkt 2 &", true);

            new Handler().postDelayed(() -> {
                binding.profileBalanced.applyingProfileDialog.setVisibility(View.GONE);
                profileState();
            }, 2000);
        });

        binding.profilePerformance.profileName.setText(getActivity().getString(R.string.performance));
        binding.profilePerformance.profileDescription.setText(getActivity().getString(R.string.performance_exp));
        binding.profilePerformance.profileIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_performance));
        binding.profilePerformance.getRoot().setOnClickListener(v -> {
            binding.profilePerformance.getRoot().post(() -> binding.profilePerformance.applyingProfileDialog.setVisibility(View.VISIBLE));
            binding.profilePerformance.getRoot().post(() -> binding.profilePerformance.applyingProfileText.setText(getActivity().getString(R.string.performance_set_up)));

            utils.runCommand("lkt 3 &", true);

            new Handler().postDelayed(() -> {
                binding.profilePerformance.getRoot().post(() -> binding.profilePerformance.applyingProfileDialog.setVisibility(View.GONE));
                profileState();
            }, 2000);
        });
    }

    private void profileState() {
        switch (utils.getProfile()) {
            case "powersave":
                binding.profilePowersave.getRoot().post(() -> binding.profilePowersave.getRoot().setChecked(true));
                binding.profileBalanced.getRoot().post(() -> binding.profileBalanced.getRoot().setChecked(false));
                binding.profilePerformance.getRoot().post(() -> binding.profilePerformance.getRoot().setChecked(false));
                break;

            case "balanced":
                binding.profilePowersave.getRoot().post(() -> binding.profilePowersave.getRoot().setChecked(false));
                binding.profileBalanced.getRoot().post(() -> binding.profileBalanced.getRoot().setChecked(true));
                binding.profilePerformance.getRoot().post(() -> binding.profilePerformance.getRoot().setChecked(false));
                break;

            case "performance":
                binding.profilePowersave.getRoot().post(() -> binding.profilePowersave.getRoot().setChecked(false));
                binding.profileBalanced.getRoot().post(() -> binding.profileBalanced.getRoot().setChecked(false));
                binding.profilePerformance.getRoot().post(() -> binding.profilePerformance.getRoot().setChecked(true));
                break;
        }
    }

    private void setLktInfo() {
        binding.managerVersion.setText(getString(R.string.manager_ver, BuildConfig.VERSION_NAME));
        binding.lktVersion.setText(getString(R.string.lkt_ver, utils.getLktVersion()));
        binding.busyboxVersion.setText(getString(R.string.busybox_ver, utils.getBusyboxVersion()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).getSupportActionBar().show();
        getActivity().setTitle(R.string.app_name);
        binding = FragmentMainBinding.inflate(inflater, container, false);

        setHasOptionsMenu(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViews();
        setLktInfo();
        viewsWorkout();
    }

    @Override
    public void onResume() {
        super.onResume();
        profileState();
        periodicUiUpdate.post(periodicUpdate);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (periodicUiUpdate != null)
            periodicUiUpdate.removeCallbacks(periodicUpdate);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        binding = null;
    }
}
