package com.paget96.lktmanager.fragments;

import androidx.fragment.app.Fragment;

public class FragmentSettings extends Fragment {

   /* // Variables
    private SwitchMaterial darkTheme, forceEnglish;
    private SharedPreferences sharedPreferences;

    private void initializeSharedPreferences() {
        sharedPreferences = getActivity().getSharedPreferences("app_preferences", Context.MODE_PRIVATE);
    }

    private void initializeSwitches() {
        darkTheme = getActivity().findViewById(R.id.dark_theme);
        forceEnglish = getActivity().findViewById(R.id.force_english);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Settings");

        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeSharedPreferences();
        initializeSwitches();
        switchState();
        switchWorkout();
    }

    private void switchWorkout() {
        darkTheme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedPreferences.edit().putBoolean("dark_theme", b).apply();
                getActivity().recreate();
            }
        });

        forceEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedPreferences.edit().putBoolean("force_english", b).apply();
                getActivity().recreate();
            }
        });
    }

    private void switchState() {
        darkTheme.setChecked(sharedPreferences.getBoolean("dark_theme", true));
        forceEnglish.setChecked(sharedPreferences.getBoolean("force_english", false));
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_about).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }*/
}
