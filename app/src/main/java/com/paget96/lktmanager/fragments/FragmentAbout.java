package com.paget96.lktmanager.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.paget96.lktmanager.BuildConfig;
import com.paget96.lktmanager.R;
import com.paget96.lktmanager.activities.MainActivity;
import com.paget96.lktmanager.databinding.FragmentAboutBinding;
import com.paget96.lktmanager.utils.UiUtils;

public class FragmentAbout extends Fragment {

    // Variables
    private FragmentAboutBinding binding;

    private void viewState() {
        binding.versionName.setText(String.format("v%s", BuildConfig.VERSION_NAME));
    }

    private void viewsWorkout() {
        binding.inviteFriends.setOnClickListener(v -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_text) + " " +
                    "https://github.com/Magisk-Modules-Repo/legendary_kernel_tweaks");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        });

        binding.paget96.setOnClickListener(v -> UiUtils.openLink(getActivity(), "https://play.google.com/store/apps/dev?id=6924549437581780390&hl=en"));
        binding.korom42.setOnClickListener(v -> UiUtils.openLink(getActivity(), "https://github.com/korom42"));
        binding.pandemic.setOnClickListener(v -> UiUtils.openLink(getActivity(), "https://forum.xda-developers.com/member.php?s=8cc6dda250917e7fae41912774be055e&u=2902434"));

        binding.changelog.setOnClickListener(v -> UiUtils.createExplanationDialog(getActivity(), R.string.changelog, R.string.changelog_list));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).getSupportActionBar().show();
        getActivity().setTitle("About");
        binding = FragmentAboutBinding.inflate(inflater, container, false);

        setHasOptionsMenu(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewState();
        viewsWorkout();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_about).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
}
