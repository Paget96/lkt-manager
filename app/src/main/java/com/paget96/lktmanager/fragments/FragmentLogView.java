package com.paget96.lktmanager.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.paget96.lktmanager.R;
import com.paget96.lktmanager.activities.MainActivity;
import com.paget96.lktmanager.databinding.FragmentLogViewBinding;
import com.paget96.lktmanager.utils.Data;
import com.paget96.lktmanager.utils.Utils;

public class FragmentLogView extends Fragment {


    private final Utils utils = new Utils();
    // Variables
    private FragmentLogViewBinding binding;

    private void getLogs() {
        String log;
        log = utils.readFile(Data.LOG, true);

        binding.logOutput.setText(!utils.fileIsEmpty(Data.LOG, true) ? log : getActivity().getString(R.string.log_file_not_found));
        binding.maxHeightScrollView.post(() -> binding.maxHeightScrollView.fullScroll(View.FOCUS_DOWN));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).getSupportActionBar().show();
        getActivity().setTitle("Log");
        binding = FragmentLogViewBinding.inflate(inflater, container, false);

        setHasOptionsMenu(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLogs();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_about).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
}
