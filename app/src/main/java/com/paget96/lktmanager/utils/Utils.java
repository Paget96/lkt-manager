package com.paget96.lktmanager.utils;

import android.app.ActivityManager;
import android.content.Context;

import com.paget96.lktmanager.BuildConfig;
import com.topjohnwu.superuser.Shell;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class Utils {

    static {
        /* Shell.Config methods shall be called before any shell is created
         * This is the why in this example we call it in a static block
         * The followings are some examples, check Javadoc for more details */
        Shell.enableVerboseLogging = BuildConfig.DEBUG;
        Shell.setDefaultBuilder(Shell.Builder.create()
                .setTimeout(10));
    }

    public String divider = "===============================================";

    public String runCommand(String command, boolean root) {
        List<String> output;

        output = root ? Shell.su(command).exec().getOut() : Shell.sh(command).exec().getOut();

        StringBuilder sb = new StringBuilder();
        for (String s : output) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString().trim();
    }

    public void closeShell() {
        try {
            Shell shell = Shell.getCachedShell();
            if (shell != null)
                shell.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // This method is responsible for creating files from input stream
    public boolean createFile(InputStream assetFile, File path, boolean overwriteIfExist) {
        if (path.exists() && overwriteIfExist) {
            OutputStream outputStream;
            byte[] buffer;
            try {
                int size = assetFile.available();
                buffer = new byte[size];
                assetFile.read(buffer);
                outputStream = new FileOutputStream(path.getPath());
                outputStream.write(buffer);
                outputStream.close();
                path.setReadable(true, false);
                path.setWritable(true, true);
                path.setExecutable(true, false);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return (path.exists() && path.length() != 0);
        } else if (!path.exists()) {
            OutputStream outputStream;
            byte[] buffer;
            try {
                int size = assetFile.available();
                buffer = new byte[size];
                assetFile.read(buffer);
                outputStream = new FileOutputStream(path.getPath());
                outputStream.write(buffer);
                outputStream.close();
                path.setReadable(true, false);
                path.setWritable(true, true);
                path.setExecutable(true, false);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return (path.exists() && path.length() != 0);
        } else return false;
    }

    // Checks if folder exist or not.
    // If it doesn't exist create it and set proper permissions in this case 775
    // If it exist then just re-do the permission setting
    public void createDir(File file) {
        if (!file.exists())
            file.mkdirs();
    }

    public void writeFile(String file, String content, boolean append, boolean root) {
        runCommand("echo '" + content + (append ? "' >> " : "' > ") + file, root);
    }

    public String parseStringWithDefault(String stringToParse, String defaultValue) {
        return !stringToParse.trim().equals("") ? stringToParse : defaultValue;
    }

    public float parseWithDefault(String integerToParse, float defaultValue) {
        float parsed;
        try {
            parsed = !integerToParse.trim().equals("") ? Float.parseFloat(integerToParse.trim()) : defaultValue;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            parsed = defaultValue;
        }

        return parsed;
    }

    public String splitString(String string, String regex, int element) {
        try {
            return string.split(regex)[element];
        } catch (ArrayIndexOutOfBoundsException e) {
            return "";
        }
    }

    public String readFile(String file, boolean root) {
        if (fileExists(file, root)) {
            return runCommand("cat '" + file + "'", root);
        } else return "";
    }

    public boolean fileExists(String file, boolean root) {
        String result =
                runCommand("if [ -e " + file + " ]; then echo true; fi", root);
        return (result != null && result.contains("true"));
    }

    public boolean fileExists(String file) {
        File getFile = new File(file);
        return getFile.exists();
    }

    public boolean fileIsEmpty(String file, boolean root) {
        String result = runCommand("if [ -s " + file + " ]; then echo true; fi", root);
        return !(result != null && result.contains("true"));
    }

    public boolean fileIsEmpty(String file) {
        File getFile = new File(file);
        return getFile.length() == 0;
    }

    public long totalRam(Context context) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        return mi.totalMem;
    }

    public long usedRam(Context context) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        return mi.totalMem - mi.availMem;
    }

    public String getBusyboxVersion() {
        return runCommand("magisk=$(ls /data/adb/magisk/magisk || ls /sbin/magisk) 2>/dev/null;\n" +
                "    magiskVersion=$(magisk -c | cut -d':' -f1) 2>/dev/null;\n" +
                "    printMagiskVersion=$(magisk -c) 2>/dev/null\n" +
                "    case \"$magiskVersion\" in\n" +
                "        '15.'[1-9]*) # Version 15\n" +
                "        busyboxPath=/sbin/.core/img/busybox-ndk\n" +
                "        MODPATH=/sbin/.core/img/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/sbin/.core/img\n" +
                "    ;;\n" +
                "        '16.'[1-9]*) # Version 16\n" +
                "        busyboxPath=/sbin/.core/img/busybox-ndk\n" +
                "        MODPATH=/sbin/.core/img/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/sbin/.core/img\n" +
                "    ;;\n" +
                "        '17.'[1-3]*) # Version 17.1-3\n" +
                "        busyboxPath=/sbin/.core/img/busybox-ndk\n" +
                "        MODPATH=/sbin/.core/img/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/sbin/.core/img\n" +
                "    ;;\n" +
                "        '17.'[4-9]*) # Version 17.4-9\n" +
                "        busyboxPath=/sbin/.magisk/img/busybox-ndk\n" +
                "        MODPATH=/sbin/.magisk/img/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/sbin/.magisk/img\n" +
                "    ;;\n" +
                "        '18.'[0-9]*) # Version 18\n" +
                "        busyboxPath=/sbin/.magisk/img/busybox-ndk\n" +
                "        MODPATH=/sbin/.magisk/img/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/sbin/.magisk/img\n" +
                "    ;;\n" +
                "        '19.'[0-9]*) # Version 20.0-20.3\n" +
                "        busyboxPath=/data/adb/modules/busybox-ndk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "        ;;\n" +
                "        '20.'[0-9]*) # Version 20.4+\n" +
                "        busyboxPath=/data/adb/magisk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "        ;;\n" +
                "        '21.'[0-9]*) # Version 21.x\n" +
                "        busyboxPath=/data/adb/magisk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "        ;;\n" +
                "        '22.'[0-9]*) # Version 22.x\n" +
                "        busyboxPath=/data/adb/magisk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "        ;;\n" +
                "        '23.'[0-9]*) # Version 23.x\n" +
                "        busyboxPath=/data/adb/magisk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "        ;;\n" +
                "        '24.'[0-9]*) # Version 24.x\n" +
                "        busyboxPath=/data/adb/magisk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "        ;;\n" +
                "        '25.'[0-9]*) # Version 25.x\n" +
                "        busyboxPath=/data/adb/magisk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "        ;;\n" +
                "        *)\n" +
                "        busyboxPath=/data/adb/magisk\n" +
                "        MODPATH=/data/adb/modules/legendary_kernel_tweaks\n" +
                "        MODULES_PATH=/data/adb/modules\n" +
                "            echo -e \"\\n >Version: $printMagiskVersion - Unknown Magisk version.\\n >  You might encounter stability issues\"\n" +
                "        ;;\n" +
                "    esac\n" +
                "\n" +
                "\n" +
                "busybox=$(ls \"$busyboxPath/system/bin/busybox\" || ls \"$busyboxPath/system/xbin/busybox\" || ls \"$busyboxPath/busybox\") 2>/dev/null\n" +
                "    #busybox=$(ls \"$busyboxPath/busybox\") 2>/dev/null\n" +
                "    busyboxAuto=$(ls /system/bin/busybox || ls /system/sbin/busybox || ls /system/xbin/busybox || ls /sbin/busybox) 2>/dev/null\n" +
                "    busyboxManualGzip=$(ls /system/bin/gzip || ls /system/sbin/gzip || ls /system/xbin/gzip || ls /sbin/gzip) 2>/dev/null\n" +
                "    busyboxManualWget=$(ls /system/bin/wget || ls /system/sbin/wget || ls /system/xbin/wget || ls /sbin/wget) 2>/dev/null\n" +
                "    busyboxManualAwk=$(ls /system/bin/awk || ls /system/sbin/awk || ls /system/xbin/awk || ls /sbin/awk) 2>/dev/null\n" +
                "    busyboxManualSort=$(ls /system/bin/sort || ls /system/sbin/sort || ls /system/xbin/sort || ls /sbin/sort) 2>/dev/null\n" +
                "    busyboxManualTruncate=$(ls /system/bin/truncate || ls /system/sbin/truncate || ls /system/xbin/truncate || ls /sbin/truncate) 2>/dev/null\n" +
                "    if [ -n \"$busybox\" ]; then\n" +
                "        bboxname=$(\"$busybox\" | head -1 | cut -f 2 -d ' ')\n" +
                " bbCheck=$G\" ✓ \"$N$W\"$bboxname is fully supported.\"$N\n" +
                "    elif [ -n \"$busyboxAuto\" ]; then\n" +
                "        bboxname=$(echo \"$(\"$busyboxAuto\" | head -1 | cut -f 2 -d ' ')\")\n" +
                " bbCheck=$G\" ✓ \"$N$W\"$bboxname is fully supported.\"$N\n" +
                "    elif [ -n \"$busyboxManualGzip\" ] && [ -n \"$busyboxManualWget\" ] && [ -n \"$busyboxManualAwk\" ] && [ -n \"$busyboxManualSort\" ] && [ -n \"$busyboxManualTruncate\" ]; then\n" +
                "        bboxname=$(echo 'Busybox Applets') \n" +
                " bbCheck=$G\" ✓ \"$N$W\"$bboxname are supported.\"$N\n" +
                "\n" +
                "    elif [ -z \"$busybox\" ] || [ -z \"$busyboxAuto\" ] || [ -z \"$busyboxManualGzip\" ] || [ -z \"$busyboxManualWget\" ] || [ -z \"$busyboxManualAwk\" ] || [ -z \"$busyboxManualSort\" ] || [ -z \"$busyboxManualTruncate\" ]; then\n" +
                "        bboxname=$(echo $R'Not Found!'$N)\n" +
                "        echo -e $R\"[+] Busybox not found.\\n[×] Exiting...\"$N\n" +
                "  sleep 3\n" +
                "        exit\n" +
                "    fi\n" +
                "\n" +
                "echo -e $P''$N$P\"$bboxname\"$N\n" +
                "#echo -e $P\"Busybox check: $bbCheck\"$N\n" +
                "echo -e $P''$N$P\"Magisk version\n$printMagiskVersion\"$N\n", true);
    }

    public String getLktVersion() {
        try {
            return readFile(Data.LOG, true).split("\n")[0].split("LKT™")[1].trim();
        } catch (ArrayIndexOutOfBoundsException e) {
            return "--";
        }
    }

    public String getProfile() {
        try {
            return readFile(Data.LOG, true).split("\n")[1].split("PROFILE :")[1].trim();
        } catch (ArrayIndexOutOfBoundsException e) {
            return "--";
        }
    }
}
