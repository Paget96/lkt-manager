package com.paget96.lktmanager.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.paget96.lktmanager.R;

public class UiUtils {

    // Parses and open links.
    public static void openLink(Context context, String link) {
        try {
            Uri uri = Uri.parse(link);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(intent);
        } catch (ActivityNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    // Wrapper for creating explanation dialogs
    public static void createExplanationDialog(final Context context, int title, int message) {

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(context);
        LayoutInflater inflater = LayoutInflater.from(context);

        TextView titleTv, messageTv;
        MaterialButton ok;

        View sheetView = inflater.inflate(R.layout.bottom_sheet_explanation_dialog, null, false);
        mBottomSheetDialog.setContentView(sheetView);
        titleTv = mBottomSheetDialog.findViewById(R.id.tv_title);
        titleTv.setText(title);

        messageTv = mBottomSheetDialog.findViewById(R.id.tv_detail);
        messageTv.setText(message);

        ok = mBottomSheetDialog.findViewById(R.id.button_ok);
        ok.setOnClickListener(v -> mBottomSheetDialog.dismiss());

        mBottomSheetDialog.show();
    }
}
